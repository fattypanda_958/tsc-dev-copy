
## url 规范

#### 支持以下四种请求类型

- `get` 获取资源
- `post` 创建资源
- `put` 更新资源
- `delete` 删除资源
    
#### 请求 url 命名规则

- `GET /resources` `GET /resource/list` 获取资源集合
- `GET /resource/:id` 获取指定资源的所有属性
- `GET /resource/attribute/:id` 获取指定资源的指定属性
- `POST /resource` 创建新资源（创建的属性在 body 中）
- `PUT /resource/:id` 更新指定资源的属性（需要更新的属性在 body 中）
- `PUT /resource/attribute/:id` 更新指定资源的指定属性（需要更新的属性在 body 中）
- `DELETE /resuource/:id` 删除指定资源

1. `resource` 资源类型（例：`user` `role` `menu`）：使用驼峰命名
2. `attribute` 属性名称（例：`status` `name`）：使用对应资源相应的属性
3. `:id` 指定的资源
    
## mock 规范

>统一使用获取 `json` `json-schema` 进行配置，以下是常用配置

#### json

>模板定义 使用 `mock` 规则

- `String` 
    - `'name|min-max': string`
    - `'name|count': string`
- `Number`
    - `'name|+1': number`
    - `'name|min-max': number`
    - `'name|min-max.dmin-dmax': number`
- `Boolean`
    - `'name|1': boolean`
    - `'name|min-max': boolean`
- `Object`
    - `'name|count': object` 
    - `'name|min-max': object`
- `Array`
    - `'name|1': array`
    - `'name|+1': array`
    - `'name|min-max': array`
    - `'name|count': array`
- `RegExp`
    - `'name': regexp`

#### json-schema

>占位符定义 使用 `mock` 占位符

##### `Basic`
- `boolean( min?, max?, current?)` 返回一个随机的布尔值。
- `natural( min?, max?)` 返回一个随机的自然数（大于等于 0 的整数）。
- `integer( min?, max? )` 返回一个随机的整数。
- `float( min?, max?, dmin?, dmax? )` 返回一个随机的浮点数。
- `character( pool? )` 返回一个随机字符。
- `string( pool?, min?, max? )` 返回一个随机字符串。
- `range( start?, stop, step? )` 返回一个整型数组。

##### `Date`
- `date( format? )` 返回一个随机的日期字符串。
- `time( format? )` 返回一个随机的时间字符串。
- `datetime( format? )` 返回一个随机的日期和时间字符串。
- `now( unit?, format? )` 返回当前的日期和时间字符串。

##### `Image`
- `image( size?, background?, foreground?, format?, text? )` 生成一个随机的图片地址。
- `dataImage( size?, text? )` 生成一段随机的 Base64 图片编码。

##### `Color`
- `color()` 随机生成一个有吸引力的颜色，格式为 '#RRGGBB'。
- `hex()` 随机生成一个有吸引力的颜色，格式为 '#RRGGBB'。
- `rgb()` 随机生成一个有吸引力的颜色，格式为 'rgb(r, g, b)'。
- `rgba()` 随机生成一个有吸引力的颜色，格式为 'rgba(r, g, b, a)'。
- `hsl()` 随机生成一个有吸引力的颜色，格式为 'hsl(h, s, l)'。

##### `Text`
- `paragraph(min?, max? )` 随机生成一段文本。
- `cparagraph(min?, max? )` 随机生成一段中文文本。
- `sentence( min?, max? )` 随机生成一个句子，第一个单词的首字母大写。
- `csentence( min?, max? )` 随机生成一段中文文本。
- `word( min?, max? )` 随机生成一个单词。
- `cword( pool?, min?, max? )` 随机生成一个汉字。
- `title( min?, max? )` 随机生成一句标题，其中每个单词的首字母大写。
- `ctitle( min?, max? )` 随机生成一句中文标题。

##### `Name`
- `first()` 随机生成一个常见的英文名。
- `last()` 随机生成一个常见的英文姓。
- `name( middle? )` 随机生成一个常见的英文姓名。
- `cfirst()` 随机生成一个常见的中文名。
- `clast()` 随机生成一个常见的中文姓。
- `cname()` 随机生成一个常见的中文姓名。

##### `Web`
- `url( protocol?, host? )` 随机生成一个 URL。
- `protocol()` 随机生成一个 URL 协议。
- `domain()` 随机生成一个域名。
- `tld()` 随机生成一个顶级域名（Top Level Domain）。
- `email( domain? )` 随机生成一个邮件地址。
- `ip()` 随机生成一个 IP 地址。

##### `Helper`
- `capitalize( word )` 把字符串的第一个字母转换为大写。
- `upper( str )` 把字符串转换为大写。
- `lower( str )` 把字符串转换为小写。
- `pick( arr )` 从数组中随机选取一个元素，并返回。
- `shuffle( arr )` 打乱数组中元素的顺序，并返回。

##### `Miscellaneous`
- `guid()` 随机生成一个 GUID。
- `id()` 随机生成一个 18 位身份证。
- `increment( step? )` 生成一个全局的自增整数。

##### 自定义
- `phone()` 随机生成一个手机号